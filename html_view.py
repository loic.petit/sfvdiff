from itertools import groupby

from diff_transform import get_from_path, hash_change


def display_key(k, path, v1, v2):
    alt_display = None
    if len(path) == 3 and path[1] == 'Scripts':
        move = get_from_path(path + [k], v2) or get_from_path(path + [k], v1)
        if move is not None:
            alt_display = move["Name"]

    elif len(path) == 2 and path[-1] == 'HitboxEffects':
        move_lists = get_from_path(path[:1] + ["Scripts"], v2)
        script_names = ", ".join(sorted(
            {m["Name"] for ml in move_lists.values() for m in ml.values() if "Hitboxes" in m for h in
             m["Hitboxes"] if h.get("EffectIndex") == k}))
        alt_display = script_names
    return (("#%d" % k if k >= 0 else "#old-%d" % abs(k + 1)) if type(k) == int else str(k)) + (
        " <i>%s</i>" % alt_display if alt_display else "")


def format_value(value):
    if type(value) == float:
        return "%.06f" % value
    res = str(value)
    if len(res) > 300:
        res = res[:300]+"..."
    return res


def display_change(change):
    if change["type"] == "values_changed":
        if "delta" in change["change"]:
            return "change: ∆%s%s" % ("+" if change["change"]["delta"] > 0 else "-", abs(change["change"]["delta"]))
        return "change: %s → %s" % (
            format_value(change["change"]["old_value"]), format_value(change["change"]["new_value"])
        )
    if change["type"] == "dictionary_item_added":
        return "added: " + format_value(change["change"])
    if change["type"] == "set_item_added" or change["type"] == "iterable_item_added":
        return "added to list: " + format_value(change["change"])
    if change["type"] == "dictionary_item_removed":
        return "removed: " + format_value(change["change"])
    if change["type"] == "set_item_removed" or change["type"] == "iterable_item_removed":
        return "removed from list: " + format_value(change["change"])
    return change.get("type", "") + " " + str(change.get("change"))


def diff_as_html(diff, v1, v2, path='root', prefix='', full_path=[]):
    if len(diff) == 1 and "#DIFF" not in diff:
        k, content = next(iter(diff.items()))
        if len(content) == 1 and "#DIFF" not in content:
            return diff_as_html(content, v1, v2, path, prefix + display_key(k, full_path, v1, v2) + ' / ',
                                full_path + [k])
    html = "<div id='diff-" + path + "' style='padding-left: 1em; border: 1px solid black; margin-bottom:1em'>"
    if "#DIFF" in diff:
        html += "<table style='padding: 5px'><tr>"
        for change in diff["#DIFF"]:
            html += "<tr>"
            html += "<td style='padding: 5px;'>%s</td>" % display_key(change["key"], full_path, v1, v2)
            html += "<td style='padding: 5px;'>%s</td>" % display_change(change)
            html += "<tr>"
        html += "</table>"
    groups_of_keys = sorted([
        sorted(g[1] for g in group)
        for key, group in groupby(
            sorted((hash_change(v), k) for k, v in diff.items() if k != '#DIFF'),
            key=lambda x: x[0]
        )
    ])
    for keys in groups_of_keys:
        new_path = path + '-' + str(keys[0])
        html += "<h4 onclick='$(\"#diff-" + new_path + "\").toggle()' style='cursor: pointer'>" + prefix + "<br>".join(
            display_key(k, full_path, v1, v2) for k in keys
        ) + "</h4>"
        html += diff_as_html(diff[keys[0]], v1, v2, new_path, "", full_path + [keys[0]])
    html += "</div>"
    return html
