import os
import pickle
import re
import sys
import time
from multiprocessing.spawn import freeze_support

sys.path.append(os.path.abspath(os.path.dirname("__file__")))

from deepdiff import DeepDiff
from diff_transform import transform_diff
from list_diff import clean_diff

from load import load_char

output_path = "result/"
char_path = "D:/Steam/SteamApps/common/StreetFighterV/StreetFighterV/Content/Paks/moves/StreetFighterV/Content/Chara"
chars = sorted(a for a in os.listdir(char_path) if a != "NDK")
versions = {re.sub("s([0-9\.]*)-.*","\\1", version): {line[:3]: line[4:] for line in open("../sfv-simulator/versions/"+version).read().strip().split("\n")} for version in os.listdir("../sfv-simulator/versions") if version[-3:] == "txt"}

from multiprocessing import Pool

def compute_char(c):
    print(c)
    previous = None
    previous_data = None
    for version, by_char in sorted(versions.items()):
        current = by_char.get(c)
        if current == previous:
            continue
        path_diff = output_path+"diffs/%s_%s_%s.bin" % (c, previous, current)
        if previous is not None and not os.path.exists(path_diff): # and current >= '032':
            if previous_data is None:
                print("\t\t", "Loading ", previous)
                previous_data = load_char(char_path, c, previous)
            print("\t\t", "Loading ", current)
            print("\t\t", "Computing diff ", previous, current)
            current_data = [1]
            current_data = load_char(char_path, c, current)
            diff = DeepDiff(previous_data, current_data, exclude_paths=["root['version']"])
            diff = transform_diff(clean_diff(diff, previous_data, current_data))
            if len(diff):
                pickle.dump(diff, open(path_diff, "wb"))
            elif os.path.exists(path_diff):
                os.unlink(path_diff)
        else:
            current_data = None
        previous = current
        previous_data = current_data
        print("\t", version, current)
    return c


if __name__ == '__main__':
    freeze_support()
    t = time.time()

    # for c in chars:
    #     compute_char(c)
    #     print(c, "is ready")
    with Pool(20) as pool:
        for c in pool.imap_unordered(compute_char, chars):
            print(c, "is ready")
    t = time.time() - t
    print("DIFFs generated in %f seconds" % t)
    exit(0)
# diff = {}
