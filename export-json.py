import json
import os
import re

import numpy

from load import load_char
from model_transform.util import Flag


def serialize_objects(a):
    if isinstance(a, Flag):
        return list(a.sorted)
    if hasattr(a, "__dict__"):
        return a.__dict__
    if hasattr(a, "asdict"):
        return a.asdict()
    if isinstance(a, set):
        return list(a)
    if isinstance(a, numpy.float32):
        return float(a)
    if isinstance(a, numpy.bool_):
        return bool(a)
    if isinstance(a, numpy.ndarray):
        return list(a)
    raise Exception("Cant serialize object %s" % str(a))


output_path = "result/"
char_path = "D:/Steam/SteamApps/common/StreetFighterV/StreetFighterV/Content/Paks/moves/StreetFighterV/Content/Chara"
chars = sorted(a for a in os.listdir(char_path) if a != "NDK")
versions = {re.sub("s([0-9\.]*)-.*","\\1", version): {line[:3]: line[4:] for line in open("../sfv-simulator/versions/"+version).read().strip().split("\n")} for version in os.listdir("../sfv-simulator/versions") if version[-3:] == "txt"}

for c, current in versions["6.031"].items():
    if c != "Z40":
        continue
    current_data = load_char(char_path, c, current)
    #with open(output_path+"json/%s.json" % (c), "w") as f:
    #    json.dump(current_data, f, default=serialize_objects)
    print("\t", c, current)

