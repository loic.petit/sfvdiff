function shiftPosition(posX, posY, container, tick) {
    var bsX = (posX || [])[tick] || 0;
    var bsY = (posY || [])[tick] || 0;
    $("#"+container+"-shift").css("left", (bsX * 100) + "px").css("bottom", (bsY * 100) + "px");
    return [bsX, bsY];
}
var currentTick = 0;
function getCoord(selector, left) {
    var res = (Math.max(...$("#position-shift "+selector+":visible").map(function(i, e) { return $(e).offset().left + $(e).width(); })) - left)/100.0;
    if (isFinite(res)) {
        return res;
    }
    return null;
}

function formatRange(label, hitRange, shiftX) {
    if (hitRange === null)
        return "";
    return label+": " + (+hitRange.toFixed(5)) + "<br/>&nbsp;&nbsp;&nbsp;&nbsp;(relative: " + (+(hitRange - shiftX).toFixed(5)) + ")<br/>";
}

function onTickClick(tick, gridElement) {
    if (tick >= totalTicks || tick < 0) return;
    currentTick = tick;
    $(".temporary").remove();
    $(".grid").removeClass("selected");
    gridElement.addClass("selected");
    $(".box").hide();
    var baseUrl = window.location.href.split('#')[0];
    window.location.replace( baseUrl + "#"+ (tick+1) );
    $("#submenu .dropdown-menu a").each(function(i, e) {
        $(e).attr("href", $(e).attr("href").replace(/#.*/, "") + "#"+(tick+1));
    });
    var shiftChanged = false;
    var boxShiftXLabel = "BoxShiftX";
    var boxShiftYLabel = "BoxShiftY";
    for (var p in positions) {
        if (p.startsWith("BoxShiftX") && p.indexOf("not") >= 0) {
            boxShiftXLabel = p;
        } else if (p.startsWith("BoxShiftY") && p.indexOf("not") >= 0) {
            boxShiftYLabel = p;
        }
    }
    var bs = shiftPosition(positions[boxShiftXLabel], positions[boxShiftYLabel], "hitbox", tick);
    var s = shiftPosition(positions["X"], positions["Y"], "position", tick);
    if (oldPositions) {
        var obs = shiftPosition(oldPositions[boxShiftXLabel], oldPositions[boxShiftYLabel], "old-hitbox", tick);
        var os = shiftPosition(oldPositions["X"], oldPositions["Y"], "old-position", tick);
        shiftChanged = bs[0] !== obs[0] || bs[1] !== obs[1] || s[0] !== os[0] || s[1] !== os[1];
    }
    for (var box in ticksBox) {
        var tickCheck = ticksBox[box];
        var $el = $("#" + box);
        if (tickCheck[0] <= tick && tick < tickCheck[1]) {
            $el.show();
            if (box.indexOf("-old") >= 0) continue;
            var $oldEl = $("#" + box+"-old");
            if ($oldEl.length) {
                $oldEl.show();
            } else {
                if (shiftChanged) {
                    $el.clone().attr("id", "#" + box+"-old").appendTo("#old-hitbox-shift").addClass("temporary").addClass("old");
                }
            }
        } else if (tickCheck.length > 2 && tickCheck[2] <= tick && tick < tickCheck[3]) {
            $el.clone().attr("id", "#" + box+"-old").appendTo("#old-hitbox-shift").addClass("temporary").addClass("old").show();
        }
    }
    for (var i = 0 ; i < states.length ; i++) {
        if ($("#activate-state-"+states[i]).prop('checked')) {
            $(".if-not-state-"+states[i]).hide();
        } else {
            $(".if-state-"+states[i]).hide();
        }
    }
    var left = $(".zero").offset().left;
    var shiftX = ($(".cross-y").offset().left - left)/100.0;
    var rangeBox = "<h4>Ranges</h4>Position: "+ (+shiftX.toFixed(5))+"<br>";
    var hitRange = getCoord(".hit", left);
    rangeBox += formatRange("Hit", hitRange, shiftX);
    var throwRange = getCoord(".hurt.throw", left);
    rangeBox += formatRange("Throw", throwRange, shiftX);
    var projectileRange = getCoord(".hurt.projectile", left);
    rangeBox += formatRange("Projectile", projectileRange, shiftX);
    var strikeRange = getCoord(".hurt.strike", left);
    rangeBox += formatRange("Strike", strikeRange, shiftX);
    var range = $("<div id='rangebox'>").html(rangeBox);
    $("#rangebox").remove();
    range.prependTo($(".box-container"));
}
function deselect(e) {
    e.stopPropagation();
    $(".box").removeClass("selected");
    $(".timeline").removeClass("selected");
}

var grid = "";
for (var i = 0; i < totalTicks; i++) {
    grid += "<div class='grid"+(i > interruptFrame ? " interrupt" : "")+"' style='left:" + (i * 30) + "px;'>" + (i+1) + "</div>";
}
grid = $(grid);
grid.each(function (tick, element) {
    element = $(element);
    element.click(function (e) {
        // deselect(e);
        onTickClick(tick, element);
    });
});
var tlContainer = $(".timeline-container");
tlContainer.on("click", function(e) {
    var offset = $(this).offset();
    var tick = Math.floor((e.clientX + tlContainer[0].scrollLeft  - offset.left)/30);
    // deselect(e);
    onTickClick(tick, $(grid[tick]));
});
$(".timeline").on("click", function(e) {
    var $el = $(this);
    var targetBox = $el.attr("id").substr(3);
    if (targetBox.indexOf("box") < 0) {
        return;
    }
    var alreadySelected = $el.hasClass("selected");
    deselect(e);
    if (!alreadySelected) {
        $el.addClass("selected");
        $("#" + targetBox).addClass("selected");
        $("#" + targetBox+"-old").addClass("selected");
    }
});
$(".property").on("click", function(e) { e.stopPropagation(); });
$(".script-cancel-list").on("click", function(e) { e.stopPropagation(); });
$(".timeline-container > div:first-child").prepend(grid);
$(".unknown").hide();
$(function () {
  $('[data-toggle="tooltip"]').tooltip({container: 'body'})
});
var sparklineDrag = false;
function selectFromSparkline(ev) {
    var sparkline = ev.sparklines[0],
        region = sparkline.getCurrentRegionFields();
    var tick = region.x;
    if (tick < totalTicks)
        onTickClick(tick, $(grid[tick]));
}

var sparkConfig = {
    type: 'line',
    height: 50,
    width: Math.min(totalTicks * 4, 300),
    chartRangeMin: 0,
    tooltipOffsetX: -100,
    tooltipFormatter: function (a, b,c) {
        if (c.y === null) {
            return "tick "+(c.x+1)+": no value";
        }
        return "tick "+(c.x+1)+": "+c.y.toFixed(6);
    }
};
var altSparkConfig = {
    type: 'line',
    composite: true,
    fillColor: false,
    lineColor: "orange",
    chartRangeMin: 0,
    tooltipFormatter: function (a, b,c) {
        if (c.y !== null) {
            return "<br/>was " + c.y.toFixed(6);
        }
        return "";
    }
};
for (var p in positions) {
    var $el = $(document.getElementById("sparkline-"+(p.toLowerCase()).replace(/ /g, "_").replace("(", "").replace(")", "")));
    var max = null;
    var min = 0;
    for (var i = 0, l = positions[p].length ; i < l ; i++) {
        if (max == null || max < positions[p][i])
            max = positions[p][i];
        if (min == null || min > positions[p][i])
            min = positions[p][i];
    }
    if (oldPositions && oldPositions[p]) {
        for (var i = 0, l = oldPositions[p].length ; i < l ; i++) {
            if (max == null || max < oldPositions[p][i])
                max = oldPositions[p][i];
            if (min == null || min > oldPositions[p][i])
                min = oldPositions[p][i];
        }
    }
    sparkConfig.chartRangeMax = max;
    altSparkConfig.chartRangeMax = max;
    sparkConfig.chartRangeMin = min;
    altSparkConfig.chartRangeMin = min;
    $el.sparkline(positions[p], sparkConfig);
    //console.log(positions[p], sparkConfig);
    if (oldPositions && oldPositions[p]) {
        if (oldPositions[p].length === 0) {
            oldPositions[p] = positions[p];
        }
        var erase = false;
        var erasedPositions = [];
        for (var i = 0, l = oldPositions[p].length ; i < l ; i++) {
            var e = oldPositions[p][i];
            var newE = positions[p][i];
            if (typeof newE === "undefined")
                newE = null;
            if (e === newE) {
                if (erase) {
                    e = null;
                }
                erase = true;
            } else {
                if (erase) {
                    erasedPositions[i-1] = oldPositions[p][i-1];
                }
                erase = false;
            }
            erasedPositions.push(e);
        }
        $el.sparkline(erasedPositions, altSparkConfig);
    }
}

$('.sparklines').bind("mousedown", function() {
    sparklineDrag = true;
}).bind("mouseup", function() {
    sparklineDrag = false;
}).bind("mouseout", function() {
    sparklineDrag = false;
}).bind('sparklineClick', function(ev) {
    sparklineDrag = true;
    selectFromSparkline(ev);
    sparklineDrag = false;
}).bind('sparklineRegionChange', function(ev) {
    if (sparklineDrag) selectFromSparkline(ev);
});

if (typeof(states) !== "undefined" && states.length) {
    var stateBox = "<h4>States</h4> ";
    for (var i = 0; i < states.length; i++) {
        stateBox += "<label>" + states[i] + " <input type='checkbox' class='activate-state' id='activate-state-" + states[i] + "'></label> ";
    }
    var state = $("<div id='statebox'>").html(stateBox);
    state.prependTo($(".box-container"));
    $(".activate-state").each(function(i, e) {
        $(e).click(function() {
            onTickClick(currentTick, $(grid[currentTick]));
        });
    });
}
var tick = 1;
if (location.hash) {
    var hash = parseInt(location.hash.substring(1));
    if (!isNaN(hash) && 1 <= hash && hash <= grid.length) {
        tick = hash;
    }
}
onTickClick(tick - 1, $(grid[tick - 1]));
$(window).on('hashchange', function() {
    if (location.hash) {
        var hash = parseInt(location.hash.substring(1));
        if (!isNaN(hash) && 1 <= hash && hash <= grid.length) {
            onTickClick(hash - 1, $(grid[hash - 1]));
        }
    }
});
$(document.body).keydown(function(e) {
   if (e.originalEvent.key == "ArrowLeft") {
       onTickClick(currentTick - 1, $(grid[currentTick >= 1 ? currentTick-1 : 0]));
   } else if (e.originalEvent.key == "ArrowRight") {
       onTickClick(currentTick + 1, $(grid[currentTick < totalTicks ? currentTick+1 : 0]));
   }
});