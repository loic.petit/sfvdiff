from view.render.jinja import render
from view.render.version_menu import char_label

with open("result/about.html", "w", encoding='utf-8') as f:
    f.write(render("about.html", title="", menu_data={}, chars=char_label))

with open("result/help.html", "w", encoding='utf-8') as f:
    f.write(render("help.html", title="", menu_data={}, chars=char_label))
