from collections import defaultdict

from diff_transform import hash_change


def display_change(change):
    if change["type"] == "type_changes":
        if change["change"]["old_value"] is None:
            return str(change["key"]) + " " + "created_instance"
        elif change["change"]["new_value"] is None:
            return str(change["key"]) + " " + "deleted_instance"
    return str(change["key"]) + " " + change["type"] + " " + str(change["change"])


def print_diff(char_diff, indent=""):
    grouped_changes = defaultdict(lambda: [])
    changes = dict()
    for k, v in sorted(char_diff.items()):
        prefix = str(k)
        while len(v) == 1 and "#DIFF" not in v:
            k, v = next(iter(v.items()))
            prefix = str(prefix) + "/" + str(k)
        if "#DIFF" in v:
            h = hash_change(v["#DIFF"])
            changes[h] = v["#DIFF"]
            grouped_changes[h].append(prefix)
        v = {ek: ev for ek, ev in v.items() if ek != "#DIFF"}
        if len(v):
            print(indent + str(prefix))
            print_diff(v, indent + "    ")
    for keys, h in sorted((keys, h) for h, keys in grouped_changes.items()):
        print(indent + ", ".join(keys))
        for change in changes[h]:
            print(indent + "    " + display_change(change))
