import os
import pickle
import re
import time
import io

import sys
from multiprocessing import Pool
from multiprocessing.spawn import freeze_support

sys.path.append(os.path.abspath(os.path.dirname("__file__")))

from deepdiff import DeepDiff

from diff_transform import get_from_path, transform_diff
from list_diff import clean_diff
from load import load_char
from model_transform.transform import char_alias
from view.render.hitbox_effect import render_hitbox_effect
from view.render.jinja import render
from view.render.move import render_move
from view.render.routing import he_path, script_path, move_path
from view.render.script import render_script
from view.render.version_menu import render_version_menu, char_label


from zipfile import ZipFile, ZIP_DEFLATED


class MockZip:
    def __init__(self, *args, **kwargs):
        return

    @staticmethod
    def open(path, mode="w"):
        path = "result/"+path
        return open(path, "wb")

    @staticmethod
    def getinfo(path):
        path = "result/"+path
        if os.path.exists(path):
            return True
        raise KeyError("File does not exists")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return


class FileCache:
    def __init__(self):
        self.f = io.BytesIO()
        self.data = None

    def __enter__(self):
        return self.f.__enter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.data = self.f.getvalue()
        ret = self.f.__exit__(exc_type, exc_val, exc_tb)
        self.f = None
        return ret


class FileStorage:
    def __init__(self, *args, **kwargs):
        self.cache = dict()
        return

    def open(self, path, mode="w"):
        self.cache[path] = FileCache()
        return self.cache[path]

    def getinfo(self, path):
        if path in self.cache:
            return True
        raise KeyError("File does not exists")

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        return

# output_path = "C:/Users/WydD/Desktop/export/"
output_path = ""
basepath = "D:/Steam/SteamApps/common/StreetFighterV/StreetFighterV/Content/Paks/moves/StreetFighterV/Content/Chara"


def extract_version_map(char_path):
    chars = sorted(a for a in os.listdir(char_path) if a != "NDK")
    versions = {
        (re.sub("s([0-9\.]*)-.*", "\\1", version), re.sub("s([0-9\.]*)-(.*)\\.txt", "\\2", version)): {
            char_alias.get(line[:3], line[:3]): line[4:]
            for line in open("../sfv-simulator/versions/" + version).read().strip().split("\n")}
        for version in os.listdir("../sfv-simulator/versions")
        if version[-3:] == "txt"
    }
    res = {}
    for c in chars:
        load_char_name = c
        c = char_alias.get(c, c)
        all_versions = {}
        res[c] = all_versions
        previous = None
        last_added = None
        for version, by_char in sorted(versions.items()):
            current = by_char.get(c)
            if current == previous:
                continue
            path_diff = "result/diffs/%s_%s_%s.bin" % (
            load_char_name, previous, current) if previous is not None else None
            if previous is not None and not os.path.exists(path_diff):
                previous = current
                continue

            all_versions[current] = (version[0], version[1], current, last_added, path_diff, load_char_name)
            last_added = current
            previous = current
    return res


def write_move(zf, menu_data, output, data, diff):
    moves_diff = get_from_path(["BCM", "Moves"], diff) or {}
    moves = get_from_path(["BCM", "Moves"], data) or {}
    input = get_from_path(["BCM", "Inputs"], data) or {}
    charges = get_from_path(["BCM", "Charges"], data) or {}
    for k, move in moves.items():
        href = move_path(move)
        menu_data["path"] = href
        rendered = render_move(move, moves_diff.get(k, {}), menu_data, input, charges, data)
        with zf.open(output + href, "w") as f:
            f.write(rendered.encode("utf-8"))


def write_hitbox_effects(zf, menu_data, output, data, diff, path, **kwargs):
    he_diff = get_from_path(path, diff) or {}
    heffects = get_from_path(path, data) or {}
    for k, he in heffects.items():
        href = he_path(k, **kwargs)
        menu_data["path"] = href
        rendered = render_hitbox_effect(k, he, he_diff.get(k, {}), menu_data)
        with zf.open(output + href, "w") as f:
            f.write(rendered.encode("utf-8"))


def write_scripts(zf, menu_data, output, data, diff, before, path, **kwargs):
    script_diff = get_from_path(path, diff) or {}
    scripts = get_from_path(path, data) or {}
    old_version = get_from_path(path, before) or {}
    for k, script in scripts.items():
        href = script_path(script, **kwargs)
        menu_data["path"] = href
        rendered = render_script(script, script_diff.get(k, {}), old_version.get(k), menu_data, data=data, **kwargs)
        with zf.open(output + href, "w") as f:
            f.write(rendered.encode("utf-8"))


def get_output_dir(char, version, zf):
    output = output_path + char + "/" + version + "/"
    if type(zf) is ZipFile or type(zf) is FileStorage:
        return output
    if not os.path.exists(output_path + char):
        os.mkdir(output_path + char)

    if not os.path.exists(output):
        os.mkdir(output)
    if not os.path.exists(output + "scripts"):
        os.mkdir(output + "scripts")
    if not os.path.exists(output + "hiteffects"):
        os.mkdir(output + "hiteffects")
    if not os.path.exists(output + "moves"):
        os.mkdir(output + "moves")
    return output


def write_version(char_path, char_versions, char, version, zf):
    char_version = char_versions[char][version]
    data = load_char(char_path, char_version[-1], version)
    if char_version[4]:
        before = load_char(char_path, char_version[-1], char_version[3])
        diff = pickle.load(open(char_version[4], "rb"))
    else:
        before = None
        diff = {}

    menu_data = {
        "char": data["char"],
        "char_label": char_label[data["char"]],
        "char_versions": char_versions[data["char"]],
        "path": "index.html"
    }
    try:
        zf.getinfo(output_path + data["char"] + "/" + menu_data["path"])
    except KeyError:
        with zf.open(output_path + data["char"] + "/" + menu_data["path"], "w") as f:
            f.write(render("version-selection.html", title="", menu_data=menu_data).encode("utf-8"))

    output = get_output_dir(data["char"], data["version"], zf)
    menu_data["version"] = data["version"]
    menu_data["version_label"] = char_versions[data["char"]][data["version"]]
    with zf.open(output + menu_data["path"], "w") as f:
        f.write(render_version_menu(data, diff, menu_data).encode("utf-8"))

    t = time.time()
    write_move(zf, menu_data, output, data, diff)
    t = time.time() - t
    print(t, "seconds to write moves")

    t = time.time()
    write_hitbox_effects(zf, menu_data, output, data, diff, ["BAC", "HitboxEffects"])
    write_hitbox_effects(zf, menu_data, output, data, diff, ["BAC_eff", "HitboxEffects"], projectile=True)
    t = time.time() - t
    print(t, "seconds to write hiteffects")

    t = time.time()
    write_scripts(zf, menu_data, output, data, diff, before, ["BAC", "Scripts", "Normal"])
    write_scripts(zf, menu_data, output, data, diff, before, ["BAC", "Scripts", "Alternate"], alt=True)
    write_scripts(zf, menu_data, output, data, diff, before, ["BAC_eff", "Scripts", "Normal"], projectile=True)
    t = time.time() - t
    print(t, "seconds to write script")


def build_char(args):
    char, version_map = args
    versions = version_map[char]
    output = FileStorage()
    for version, version_content in sorted(versions.items()):
        #if version_content[-1] not in {"KEN", "Z29", "Z35", "Z37"}:
        #    continue
        #if version_content[0] != "5.001":
        #    continue
        print(char, version, version_content)
        write_version(basepath, version_map, char, version, output)
    return char, output.cache


if __name__ == '__main__':
    freeze_support()
    t = time.time()
    with ZipFile("result/dump.zip", "w", compression=ZIP_DEFLATED) as zf:
        with zf.open(output_path + "about.html", "w") as f:
            f.write(render("about.html", title="", menu_data={}, chars=char_label).encode("utf-8"))
        with zf.open(output_path + "help.html", "w") as f:
            f.write(render("help.html", title="", menu_data={}, chars=char_label).encode("utf-8"))
        with zf.open(output_path + "index.html", "w") as f:
            f.write(render("main.html", title="", menu_data={}, chars=char_label).encode("utf-8"))

        version_map = extract_version_map(basepath)
        with Pool(20) as p:
            for char, output in p.imap_unordered(build_char, sorted([(c, version_map) for c in version_map.keys()])):
                print(char, " is ready, writing %d files..." % len(output))
                for path, f in output.items():
                    with zf.open(path, "w") as o:
                        o.write(f.data)
                print(char, " is done")

    t = time.time() - t
    print("DIFFs generated in %f seconds" % t)