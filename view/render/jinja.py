import htmlmin
from jinja2 import Environment, FileSystemLoader
import re

env = Environment(
    loader=FileSystemLoader('view/template/')
)
env.filters['removedirections'] = lambda x: [i for i in x if i not in {'DOWN', 'UP', 'BACK', 'FORWARD', 'NEUTRAL'}]
env.filters['directions'] = lambda x: [i for i in x if i in {'DOWN', 'UP', 'BACK', 'FORWARD', 'NEUTRAL'}]


def render(template_name, **kwargs):
    template = env.get_template(template_name)
    return re.sub("\\s+", " ", template.render(**kwargs))
    # return htmlmin.minify(
    #     template.render(**kwargs),
    #     remove_comments=True, remove_all_empty_space=True, reduce_boolean_attributes=True
    # )
