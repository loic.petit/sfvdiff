import re

from diff_transform import get_from_path


def get_script_routing(data, script_index, alt=False, projectile=False):
    if script_index < 0:
        return "./", {"Name": "Missing Script!", "Index": script_index}
    scripts = get_from_path(["BAC" if not projectile else "BAC_eff", "Scripts"], data) or {}
    if alt and (projectile or script_index not in scripts["Alternate"]):
        alt = False
    script = scripts["Normal" if not alt else "Alternate"].get(script_index)
    if script is None:
        return "./", {"Name": "Missing Script!", "Index": script_index}
    return script_path(script, alt=alt, projectile=projectile), script


def script_path(script, alt=False, projectile=False, **kwargs):
    alt_path = ""
    if alt:
        alt_path = "ALT_"
    elif projectile:
        alt_path = "PROJ_"
    return "scripts/%s%d_%s.html" % (alt_path, script["Index"], re.sub("[^A-Za-z0-9_]", "_", script["Name"]))


def move_path(move, **kwargs):
    return "moves/%s.html" % re.sub("[^A-Z0-9_]", "_", move["Name"].upper())


def he_path(he_id, projectile=False, **kwargs):
    return "hiteffects/%s%d.html" % ("PROJ_" if projectile else "", he_id)
