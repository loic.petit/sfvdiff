from view.render.jinja import render


def render_hitbox_effect(key, hitbox_effect, diff, menu_data):
    attributes = [
        "Type", "Script", "Flag", "Damage", "Stun", "EXAttacker", "EXDefender", "VGauge",
        "HitFreezeAttacker", "HitFreezeDefender", "MainRecoveryDuration", "KnockdownDuration", "KnockBack", "FallSpeed",
        "JuggleStart", "DamageScalingGroup", "VTimer"
    ]
    situations = ["HIT_STAND", "HIT_CROUCH", "HIT_OTG", "HIT_AIR", "COUNTERHIT_STAND", "COUNTERHIT_CROUCH", "COUNTERHIT_OTG",
     "COUNTERHIT_AIR", "GUARD_STAND", "GUARD_CROUCH"]
    unknowns = {attrib for situation in situations for attrib in hitbox_effect[situation]["Unknown"].keys()}
    unknowns_in_diff = {attrib for situation in situations if situation in diff and "Unknown" in diff[situation] for attrib in diff[situation]["Unknown"].get("#DIFF", {}).keys()}
    unknowns |= unknowns_in_diff
    effect_rows = {}
    for attrib in attributes+list(unknowns):
        is_uk = attrib in unknowns
        grouped = extract_he(attrib, diff, hitbox_effect, is_uk, situations[0])
        colspan = 1
        row = []
        for situation in situations[1:]:
            current = extract_he(attrib, diff, hitbox_effect, is_uk, situation)
            if grouped == current:
                colspan += 1
            else:
                row.append((colspan, grouped[0], diff_as_value(grouped[1])))
                colspan = 1
                grouped = current
        row.append((colspan, grouped[0], diff_as_value(grouped[1])))
        effect_rows[attrib] = row
    return render(
        "hitbox_effect.html",
        title="Hitbox Effect %d" % key,
        menu_data=menu_data,
        attributes=attributes, effect_rows=effect_rows, unknowns=sorted(unknowns),
        has_unknown_diff=len(unknowns_in_diff) > 0
    )


def extract_he(attrib, diff, hitbox_effect, is_uk, situations):
    he = hitbox_effect[situations]
    diff_he = diff.get(situations, {})
    if not is_uk:
        grouped = (he.get(attrib), diff_he.get("#DIFF", {}).get(attrib))
    else:
        grouped = (he["Unknown"].get(attrib), diff_he.get("Unknown", {}).get("#DIFF", {}).get(attrib))
    return grouped


def diff_as_value(grouped):
    if not grouped:
        return None
    change_type = grouped["type"].split("_")[-1]
    if grouped["type"] == "values_changed":
        return change_type, grouped["change"]["old_value"]
    return change_type, None
