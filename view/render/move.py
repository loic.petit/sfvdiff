from view.render.jinja import render
from view.render.routing import get_script_routing
from view.render.script import append_property_diff
from view.render.util import extract_diff_for_view


def render_move(move, diff, menu_data, inputs, charges, data):
    property_diff = extract_diff_for_view(diff)
    append_property_diff(diff, property_diff, "Unknown")
    append_property_diff(diff, property_diff, "InputMatch")
    return render(
        'move.html',
        title=move["Name"],
        menu_data=menu_data,
        motions=inputs, charges=charges, script=get_script_routing(data, move["Script"], alt=(move.get("Stance") == "Alternative")),
        move=move, property_diff=property_diff
    )

