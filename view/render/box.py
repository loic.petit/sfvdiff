import json


def get_box_style(path, h):
    res = ""
    if "ActiveOnStates" in h:
        for st in h["ActiveOnStates"]:
            res += "if-state-" + st + " "
    if "InactiveOnStates" in h:
        for st in h["InactiveOnStates"]:
            res += "if-not-state-" + st + " "
    if path == "Hurtboxes":
        if h.get("Type") == "Armor":
            return res + "hurt armor"
        return res + "hurt "+" ".join([e.lower() for e in h["Sensitivity"]])
    if path == "Pushboxes":
        return res + "push"
    if h["HitType"] == "Proximity Guard":
        return res + "proximity"
    return res + "hit"


def render_boxes(script, diff):
    render = []
    old_render = []
    ticks_box = {}
    for path in ["Hitboxes", "Hurtboxes", "Pushboxes"]:
        path_diff = diff.get(path, {})
        for n, h in enumerate(script.get(path, [])):
            property_diff = path_diff.get(n, {}).get("#DIFF", {})
            if "X" in property_diff or "Y" in property_diff or "Width" in property_diff or "Height" in property_diff:
                render_box({
                    "TickStart": property_diff["TickStart"]["change"]["old_value"] if "TickStart" in property_diff else h["TickStart"],
                    "TickEnd": property_diff["TickEnd"]["change"]["old_value"] if "TickEnd" in property_diff else h["TickEnd"],
                    "HitType": h.get("HitType"),
                    "Sensitivity": h.get("Sensitivity"),
                    "Type": h.get("Type"),
                    "X": property_diff["X"]["change"]["old_value"] if "X" in property_diff else h["X"],
                    "Y": property_diff["Y"]["change"]["old_value"] if "Y" in property_diff else h["Y"],
                    "Width": property_diff["Width"]["change"]["old_value"] if "Width" in property_diff else h["Width"],
                    "Height": property_diff["Height"]["change"]["old_value"] if "Height" in property_diff else h["Height"],
                }, n, path, old_render, ticks_box, " old")
            box_id = render_box(h, n, path, render, ticks_box, "")
            if "TickStart" in property_diff or "TickEnd" in property_diff:
                ticks_box[box_id].append(property_diff["TickStart"]["change"]["old_value"] if "TickStart" in property_diff else h["TickStart"])
                ticks_box[box_id].append(property_diff["TickEnd"]["change"]["old_value"] if "TickEnd" in property_diff else h["TickEnd"])
        removed_items = [
            (k, change["change"])
            for k, change in path_diff.get("#DIFF", {}).items()
            if change["type"] == "iterable_item_removed"
        ]
        for n, h in removed_items:
            render_box(h, n, path, old_render, ticks_box, " removed")
    js = "var ticksBox = %s;" % json.dumps(ticks_box)
    cross = "<div class='cross-x'></div><div class='cross-y'></div>"
    render = "<div id='hitbox-shift'>%s</div>" % "".join(render)
    old_render = "<div id='old-position-shift'><div id='old-hitbox-shift'>%s</div></div>" % "".join(old_render)
    render = "<div class='zero'></div>%s<div id='position-shift'>%s%s</div>" % (old_render, render, cross)
    return render, js


def render_box(h, n, path, render, ticks_box, additional_class):
    box_id = "%s-%d" % (path.lower(), n)
    if additional_class == " old":
        box_id += "-old"
    ticks_box[box_id] = [h["TickStart"], h["TickEnd"]]
    render.append("<div id='%s' class='%s box' style='left:%dpx;bottom:%dpx;width:%dpx;height:%dpx;'></div>" % (
        box_id,
        get_box_style(path, h) + additional_class,
        round(h["X"] * 100.0), round(h["Y"] * 100.0), round(h["Width"] * 100.0), round(h["Height"] * 100.0)
    ))
    return box_id
