from diff_transform import get_from_path
from view.render.jinja import render
from view.render.routing import script_path, he_path, move_path
from view.render.util import extract_diff_for_view

char_label = {
    "ABG": "Abigail",
    "AKR": "Akira",
    "ALX": "Alex",
    "BLK": "Blanka",
    "BLR": "Vega [Claw]",
    "BRD": "Birdie",
    "BSN": "Balrog [Boxer]",
    "CDY": "Cody",
    "CMN": "Common",
    "CMY": "Cammy",
    "CNL": "Chun-Li",
    "DAN": "Dan",
    "DSM": "Dhalsim",
    "EDO": "Ed",
    "FAN": "F.A.N.G.",
    "FLK": "Falke",
    "G":   "G",
    "GIL": "Gill",
    "GOK": "Akuma",
    "GUL": "Guile",
    "HND": "E. Honda",
    "IBK": "Ibuki",
    "JRI": "Juri",
    "KEN": "Ken",
    "KGE": "Kage",
    "KLN": "Kolin",
    "KRN": "Karin",
    "LAR": "Laura",
    "LCA": "Lucia",
    "LKE": "Luke",
    "MNT": "Menat",
    "NCL": "Necalli",
    "NSH": "Nash",
    "ORO": "Oro",
    "PSN": "Poison",
    "RMK": "R. Mika",
    "RSD": "Rashid",
    "RSE": "Rose",
    "RYU": "Ryu",
    "SGT": "Sagat",
    "SKR": "Sakura",
    "STH": "Seth",
    "URN": "Urien",
    "VEG": "M. Bison [Dictator]",
    "ZGF": "Zangief",
    "ZKU": "Zeku"
}


def get_diff_classes(diff):
    diff = diff or {}
    result = {k: "changed" for k in diff if k != "#DIFF"}
    for k, v in diff.get("#DIFF", {}).items():
        result[k] = v["type"].split("_")[-1]
    return result


def as_indexed_scripts(scripts, diff, previous_version, **kwargs):
    diff = diff or {}
    res = []
    diff_classes = get_diff_classes(diff)
    removed_scripts = {k: v["change"] for k, v in diff.get("#DIFF", {}).items() if "removed" in v["type"]}
    for i in range(0, 2000):
        if i in scripts:
            res.append((scripts[i], diff_classes.get(i), script_path(scripts[i], **kwargs)))
        elif i in removed_scripts:
            res.append((removed_scripts[i], "removed", "../%s/%s" % (previous_version, script_path(removed_scripts[i], **kwargs))))
        else:
            res.append(None)
    return res


def as_move_list(data, diff):
    moves = get_from_path(["BCM", "Moves"], data) or {}
    diff = get_from_path(["BCM", "Moves"], diff) or {}
    diff_classes = extract_diff_for_view(diff)
    return sorted(((move, diff_classes.get(name), move_path(move)) for name, move in moves.items()), key=lambda x: x[0]["Index"])


def cancel_list_sort(cl_name):
    if cl_name == "GROUND":
        return 0
    if cl_name == "NEUTRAL JUMP":
        return 1
    if cl_name == "FORWARD JUMP":
        return 2
    if cl_name == "BACKWARD JUMP":
        return 3
    return int(cl_name)


def cancel_list_usage_item(m, normal_scripts, alt_scripts, diffu):
    script = (normal_scripts if m[0] == 0 else alt_scripts)[m[1]]
    return script, script_path(script, m[0] == 1), extract_diff_for_view(diffu).get(m)


def as_cancel_list_list(data, diff):
    moves = get_from_path(["BCM", "Moves"], data) or {}
    cll = get_from_path(["BCM", "CancelLists"], data) or {}
    diffu = get_from_path(["BCM", "CancelListsUsage"], diff) or {}
    cllu = get_from_path(["BCM", "CancelListsUsage"], data) or {}
    diff = get_from_path(["BCM", "CancelLists"], diff) or {}
    normal_scripts = get_from_path(["BAC", "Scripts", "Normal"], data) or {}
    alt_scripts = get_from_path(["BAC", "Scripts", "Alternate"], data) or {}
    dict_diff = extract_diff_for_view(diff)
    return sorted(
        (
            (
                name,
                [(m, extract_diff_for_view(diff.get(name, {})).get(i), move_path(moves[m])) for i, m in enumerate(cl)],
                dict_diff.get(name),
                sorted([cancel_list_usage_item(m, normal_scripts, alt_scripts, diffu.get(name, {})) for m in cllu.get(name, [])], key=lambda x: x[0]["Index"])
            )
            for name, cl in cll.items()
        ),
        key=lambda x: cancel_list_sort(x[0])
    )


def effects_map(scripts, diff, **kwargs):
    classes = get_diff_classes(diff)
    return {i: sorted({(h["Effect"], classes.get(h["Effect"]), he_path(h["Effect"], **kwargs)) for h in s["Hitboxes"] if "Effect" in h}) for i, s in scripts.items() if "Hitboxes" in s}


def render_version_menu(data, diff, menu_data):
    normal_diff = get_from_path(["BAC", "Scripts", "Normal"], diff)
    alt_diff = get_from_path(["BAC", "Scripts", "Alternate"], diff)
    proj_diff = get_from_path(["BAC_eff", "Scripts", "Normal"], diff)
    input = get_from_path(["BCM", "Inputs"], data) or {}
    charges = get_from_path(["BCM", "Charges"], data) or {}
    he_map = {
        "Normal": effects_map(data["BAC"]["Scripts"]["Normal"], get_from_path(["BAC", "HitboxEffects"], diff)),
        "Alternate": effects_map(data["BAC"]["Scripts"].get("Alternate", dict()), get_from_path(["BAC", "HitboxEffects"], diff))
    }
    previous_version = menu_data["char_versions"][data["version"]][3]
    script_list = {
        "Normal": as_indexed_scripts(data["BAC"]["Scripts"]["Normal"], normal_diff, previous_version)
    }
    if "Alternate" in data["BAC"]["Scripts"]:
        script_list["Alternate"] = as_indexed_scripts(data["BAC"]["Scripts"]["Alternate"], alt_diff,  previous_version, alt=True)
    if data["BAC_eff"] is not None:
        script_list["Projectiles"] = as_indexed_scripts(data["BAC_eff"]["Scripts"]["Normal"], proj_diff, previous_version,
                                                        projectile=True)
        he_map["Projectiles"] = effects_map(data["BAC_eff"]["Scripts"]["Normal"], get_from_path(["BAC_eff", "HitboxEffects"], diff),
                                            projectile=True)
    cancel_list_list = as_cancel_list_list(data, diff)
    return render(
        "version-menu.html",
        char_img=data["char"].lower(),
        title="",
        version=data["version"],
        menu_data=menu_data,
        cancel_lists=cancel_list_list,
        motions=input,
        charges=charges,
        char=char_label[data["char"]], bch=data["BCH"], bch_diff=extract_diff_for_view(diff.get("BCH", {})),
        moves=as_move_list(data, diff),
        script_list=script_list, effects=he_map
    )
