from typing import List, Union

import numpy as np

FLT = np.float32
ZERO = FLT(0)


class Position:
    """
    @type shift: np.ndarray
    @type coord: np.ndarray
    @type force: np.ndarray
    @type ref: np.ndarray
    @type side: bool
    """
    def __init__(self, x: Union[float, FLT], y: Union[float, FLT]):
        self.coord = [FLT(x), FLT(y)]
        self.speed = [ZERO, ZERO]
        self.temp_speed = [None, None]
        self.on_speed_change = [None, None, None, None, None, None]
        self.force = [ZERO, ZERO]
        self.temp_force = [None, None]
        self.shift = [ZERO, ZERO]
        self.ref = [FLT(x), FLT(y)]
        self.side = True


class Timeline:
    def __init__(self, cls, data):
        self.empty = data is None or len(data) == 0
        if self.empty:
            return
        self.min = 100000
        self.max = -1
        if cls is not None:
            self.instances = [cls(d) for d in data]
        else:
            self.instances = data

        self.timeline = dict()
        for d in self.instances:
            start = d["TickStart"]
            end = d["TickEnd"]
            if "ActiveOnStates" in d:
                continue
            if start < self.min:
                self.min = start
            if end > self.max:
                self.max = end
            for frame in range(start, end):
                if frame not in self.timeline:
                    self.timeline[frame] = [d]
                else:
                    self.timeline[frame].append(d)

    def __getstate__(self):
        if self.empty:
            return []
        return self.instances

    def __setstate__(self, state):
        self.__init__(None, state)

    def at(self, tick) -> List:
        if not self.empty and not (tick < self.min or tick >= self.max) and tick in self.timeline:
            return self.timeline[tick]
        return []


def shift_to_js(script, shift_type):
    shift = script.get("Positions", {}).get(shift_type)
    if not shift:
        return []
    res = [None for _ in range(0, shift[0])] + [tick if tick == tick else None for tick in shift[1]]
    res += [None for _ in range(len(res), script["TotalTicks"])]
    return res

def add_position_entries(p1, p2):
    if p1 is None:
        return p2
    if p2 is None:
        return p1
    res = list(p1)
    for i, p in enumerate(p2):
        if res[i] is None:
            res[i] = p
        elif p is not None:
            res[i] += p
    return res


def simulate_positions(script):
    positions = {k: shift_to_js(script, k) for k in script.get("Positions", {}).keys()}
    ticks = script["TotalTicks"]
    shift_x = positions.get("ShiftX")
    shift_y = positions.get("ShiftY")
    ground_level = positions.get("GroundLevel")
    for item, p in positions.items():
        if item.startswith("ShiftX") and "not" in item:
            shift_x = add_position_entries(p, shift_x)
        if item.startswith("ShiftY") and "not" in item:
            shift_y = add_position_entries(p, shift_y)
        if item.startswith("GroundLevel") and "not" in item:
            ground_level = add_position_entries(p, ground_level)
    forces = script.get("Forces")
    timeline = Timeline(None, forces)
    pos = Position(0, 0)
    result_x = []
    result_y = []
    for tick in range(0, ticks):
        sx = (shift_x[tick] or ZERO) if shift_x else ZERO
        sy = shift_y[tick] if shift_y else None
        if sy is None and shift_y:
            if tick > 0:
                sy = shift_y[tick-1]
        if not sy:
            sy = ZERO
        gl = (ground_level[tick] or ZERO) if ground_level else ZERO
        reset_temporary = set()
        alter_pos(pos, reset_temporary, tick, timeline)
        apply_forces(pos, sy, gl)
        if 0 not in reset_temporary:
            pos.temp_speed[0] = None
        if 1 not in reset_temporary:
            pos.temp_speed[1] = None
        if 2 not in reset_temporary:
            pos.temp_force[0] = None
        if 3 not in reset_temporary:
            pos.temp_force[1] = None
        result_x.append(pos.ref[0] + sx)
        result_y.append(pos.ref[1] + sy)

    positions["X"] = result_x
    positions["Y"] = result_y
    return positions


def alter_pos(pos, reset_temporary, tick, timeline):
    for f in timeline.at(tick):
        target = f["Target"]
        mode = f["Mode"]
        # if mode == "Set" and (f["TickEnd"] - f["TickStart"]) > 1:
        #     continue
        amount = FLT(f["Amount"])
        if target == "Horizontal Speed":
            if mode == "Set":
                pos.speed[0] = amount
            elif mode == "Multiply":
                pos.speed[0] *= amount
            elif mode == "Temporary":
                reset_temporary.add(0)
                pos.temp_speed[0] = amount
            elif mode == "On Speed Change":
                pos.on_speed_change[0] = amount
        elif target == "Vertical Speed":
            if mode == "Set":
                pos.speed[1] = amount
            elif mode == "Multiply":
                pos.speed[1] *= amount
            elif mode == "Temporary":
                reset_temporary.add(1)
                pos.temp_speed[1] = amount
            elif mode == "On Speed Change":
                pos.on_speed_change[1] = amount
        elif target == "Horizontal Acceleration":
            if mode == "Set":
                pos.force[0] = amount
            elif mode == "Multiply":
                pos.force[0] *= amount
            elif mode == "Temporary":
                reset_temporary.add(2)
                pos.temp_force[0] = amount
            elif mode == "On Speed Change":
                pos.on_speed_change[2] = amount
        elif target == "Vertical Acceleration":
            if mode == "Set":
                pos.force[1] = amount
            elif mode == "Multiply":
                pos.force[1] *= amount
            elif mode == "Temporary":
                reset_temporary.add(3)
                pos.temp_force[1] = amount
            elif mode == "On Speed Change":
                pos.on_speed_change[3] = amount


def apply_forces(pos, sy, gl):
    """
    Apply all forces to the p.pos

    :param p: the player state
    :param pos: the player's position
    :param sign: the current sign
    """
    # apply position changes
    pos.ref[0] += pos.speed[0] if pos.temp_speed[0] is None else pos.temp_speed[0]
    pos.ref[1] += pos.speed[1] if pos.temp_speed[1] is None else pos.temp_speed[1]

    old_sign_x = pos.speed[0] >= 0
    old_sign_y = pos.speed[1] >= 0
    # advance velocity
    pos.speed[0] += pos.force[0] if pos.temp_force[0] is None else pos.temp_force[0]
    pos.speed[1] += pos.force[1] if pos.temp_force[1] is None else pos.temp_force[1]

    new_sign_x = pos.speed[0] >= 0
    new_sign_y = pos.speed[1] >= 0

    if new_sign_x != old_sign_x:
        if pos.on_speed_change[0] is not None:
            pos.speed[0] = pos.on_speed_change[0]
            pos.on_speed_change[0] = None
        if pos.on_speed_change[3] is not None:
            pos.force[0] = pos.on_speed_change[3]
            pos.on_speed_change[3] = None
    if new_sign_y != old_sign_y:
        if pos.on_speed_change[1] is not None:
            pos.speed[1] = pos.on_speed_change[1]
            pos.on_speed_change[1] = None
        if pos.on_speed_change[4] is not None:
            pos.force[1] = pos.on_speed_change[4]
            pos.on_speed_change[4] = None

    if pos.ref[1] + sy - gl < 0:
        pos.ref[1] = gl
        pos.speed[1] = ZERO
