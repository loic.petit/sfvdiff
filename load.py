import json
import os

import bson
import re

from deepdiff import DeepDiff

from diff_transform import transform_diff
from html_view import diff_as_html
from list_diff import clean_diff
from model_transform.transform import transform_data


def bson_loads(path):
    return bson.loads(open(path, mode='rb').read())


def json_loads(path):
    json_str = open(path).read()
    json_str = re.sub(",[ \t\r\n]*}", "}", json_str)
    json_str = re.sub(",[ \t\r\n]*\]", "]", json_str)
    json_str = re.sub("}[ \t\r\n]*{", "},{", json_str)
    return json.loads(json_str)

source_load = bson_loads

#
# def transform_data(data):
#     cancel_lists = dict()
#     for cl in data["BCM"]["CancelLists"]:
#         if cl["Cancels"] is None:
#             continue
#         cancel_lists[cl["Index"]] = {c["Name"] for c in cl["Cancels"]}
#     data["BCM"]["CancelLists"] = cancel_lists
#     data["BCM"]["Moves"] = {m["Name"]: m for m in data["BCM"]["Moves"]}
#     return data


def load_char(char_path, char, version):
    version_path = char_path + "/" + char + "/BattleResource/" + version
    BCH = source_load(version_path + "/BCH_" + char + ".json").get("BCH", dict())
    BCM = None
    if os.path.exists(version_path + "/BCM_" + char + ".json"):
        BCM = source_load(version_path + "/BCM_" + char + ".json")
    BAC = source_load(version_path + "/BAC_" + char + ".json")
    BAC_eff = None
    eff_path = version_path + "/BAC_" + char + "_eff.json"
    if os.path.exists(eff_path):
        BAC_eff = source_load(eff_path)
    result = transform_data(BAC, BCM, BCH, BAC_eff, char, version)
    return result

