from collections import defaultdict

import numpy as np

from model_transform.util import read_flags


def parse_position_flag(flag):
    suffix = ""
    if flag[1] != (0, 0):
        suffix = " ("
        if flag[1][0]:
            suffix += "only in " + ", ".join(read_flags(flag[1][0], ["A", "B", "C", "D", "E", "F", "G", "H"]))
        if flag[1][1]:
            suffix += "not in " + ", ".join(read_flags(flag[1][1], ["A", "B", "C", "D", "E", "F", "G", "H"]))
        suffix += ")"
    hi_end = flag[0] >> 15
    low_end = flag[0] & 0xFFF
    unknown = "Unknown (%x)" % flag[0]
    unknown += suffix
    if hi_end & 8:
        # Relative
        if low_end != 0:
            return unknown
        if hi_end & 1:
            return "RelativeX" + suffix
        if hi_end & 2:
            return "RelativeY" + suffix
        return unknown
    if low_end == 0:
        # Shift
        if hi_end & 1:
            return "ShiftX" + suffix
        if hi_end & 2:
            return "ShiftY" + suffix
        return unknown
    if low_end & 4:
        # Shift
        if hi_end & 1:
            return "BoxShiftX" + suffix
        if hi_end & 2:
            return "BoxShiftY" + suffix
        return unknown
    if low_end & 8:
        # Shift
        if hi_end & 2:
            return "GroundLevel" + suffix
        return unknown
    return unknown


def transform_positions(positions, max_frames):
    lines = defaultdict(lambda: np.full(shape=(max_frames,), fill_value=np.nan, dtype=np.float32))
    for data in positions or []:
        if "Flag" not in data:
            continue
        flag = data["Flag"]
        states = data.get("BACVERint1", 0), data.get("BACVERint2", 0)
        line = lines[flag, states]
        value = data["Movement"]
        np.nan_to_num(line[data["TickStart"]:data["TickEnd"]], copy=False)
        line[data["TickStart"]:data["TickEnd"]] += value
    final_lines = dict()
    for flag, line in lines.items():
        flag = parse_position_flag(flag)
        nnz = np.nonzero(np.isfinite(line))[0]
        if nnz.shape[0] == 0:
            continue

        final_lines[flag] = (int(nnz[0]), line[nnz[0]: (nnz[-1] + 1)])

    return final_lines