import struct

from model_transform.bcm import read_input_mask
from model_transform.util import read_flags, transform_time_element


def parse_autocancel_condition(data):
    condition = data["Condition"] & 0xFF if type(data["Condition"]) == int else data["Condition"]
    params = data["Ints"]
    if condition == "Always" or condition == 0:
        return "Always", None
    if condition == 1:
        return "On Hit", None
    if condition == 2 or condition == "OnBlock":
        return "On Block", None
    if condition == 3:
        return "On Whiff", None
    if condition == 4:
        params = params or [0, 0, 0, 0]
        if params[3]:
            params = {
                "HitboxCount": params[0]
            }
        else:
            params = {
                "HitboxId": params[0]
            }
        return "On Specific Hit", params
    if condition == 5:
        params = params or [0, 0, 0, 0]
        if params[3]:
            params = {
                "HitboxCount": params[0]
            }
        else:
            params = {
                "HitboxId": params[0]
            }
        return "After Specific Hit", params
    if condition == 8:
        return "On Landing", None
    if condition == 9:
        return "On Camera Proximity", {"Distance": params[0] / 100 if params else "Unknown"}
    if condition == 12:
        return "On Opponent Proximity", {"Distance": params[1] / 100}
    if condition == 14:
        return "On Button", {
            "Action": {0: "Hold All", 1: "Button Interruption", 2: "Release Any", 3: "Release Two", 4: "Hold Any"}.get(params[0], "Unknown"),
            "Buttons": read_input_mask(params[1])
        }
    if condition == 15:
        return "After Armor", None
    if condition == 16 or (condition == 17 and not params):
        return "On Armor", None
    if condition == 17:
        return "On Specific Armor", {
            "Level": read_flags(params[0], ["Low", "Middle", "High", "Crouch", "Air"])
        }
    if condition == 20:
        return "If Resource?", {
            "Resource": params[0]
        } if params else None
    if condition == 24:
        return "On Input Hold", None
    if condition == 26:
        return "On Projectile Presence?", None
    return "On Unknown %d" % condition,{"Params": params}


def transform_autocancel(data):
    res = transform_time_element(data)
    unk = dict()
    if data["Unknown3"]:
        unk["Unknown3"] = data["Unknown3"]
    if data["Unknown4"]:
        unk["Unknown4"] = data["Unknown4"]
    if type(data["Condition"]) == int and data["Condition"] & 0x8000:
        res["Condition"] = data["Condition"] & 0x7FFF
        res["ActivateStates"] = read_flags(data["Unknown2"], ["A", "B", "C", "D", "E", "F", "G", "H"])
    else:
        res["Script"] = "Self" if data["MoveIndex"] < 0 else data["MoveIndex"]
        if data["Unknown2"] == 2:
            res["ScriptTime"] = data["Unknown1"]
        else:
            if data["Unknown1"]:
                res["ScriptTime"] = data["Unknown1"]
            if data["Unknown2"]:
                unk["Unknown2"] = data["Unknown2"]
    condition, params = parse_autocancel_condition(data)
    res["Condition"] = condition
    if params:
        res["Parameters"] = params
    if unk:
        res["Unknown"] = unk
    return res

