from collections import defaultdict

from model_transform.bac import transform_bac
from model_transform.bch import transform_bch
from model_transform.bcm import transform_bcm

char_alias = {
    "Z20": "KLN",
    "Z21": "GOK",
    "Z22": "EDO",
    "Z23": "MNT",
    "Z24": "ABG",
    "Z25": "ZKU",
    "Z26": "SKR",
    "Z27": "BLK",
    "Z28": "FLK",
    "Z29": "CDY",
    "Z30": "G",
    "Z31": "SGT",
    "Z32": "KGE",
    "Z33": "PSN",
    "Z34": "HND",
    "Z35": "LCA",
    "Z36": "GIL",
    "Z37": "STH",
    "Z38": "DAN",
    "Z39": "RSE",
    "Z40": "ORO",
    "Z41": "AKR",
    "Z42": "LKE"
}


def transform_data(BAC, BCM, BCH, BAC_eff, char, version):
    cancel_lists_usage = defaultdict(lambda: set())
    result = {
        "BAC": transform_bac(BAC, cancel_lists_usage),
        "BCM": transform_bcm(BCM),
        "BCH": transform_bch(BCH),
        "BAC_eff": transform_bac(BAC_eff, None) if BAC_eff else None,
        "char": char_alias.get(char, char),
        "version": version
    }
    result["BCM"]["CancelListsUsage"] = cancel_lists_usage
    return result