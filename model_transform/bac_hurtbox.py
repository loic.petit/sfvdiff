from model_transform.bac_misc import transform_box
from model_transform.util import read_flags


def transform_hurtbox(data):
    res = transform_box(data)
    unk = dict()
    res["Sensitivity"] = read_flags(data["Flag1"], ["Strike", "Projectile", "Throw"])
    if data["Unknown6"] & 0x8000:
        # just ignore this case for now? ...
        data["Unknown6"] &= 0x7FFF
    if data["Unknown6"]:
        if data["Unknown6"] < 0:
            unk["Unknown6"] = data["Unknown6"]
            print("***************** Unexpected data found in Unknown6 %d**************************" % data["Unknown6"])
        else:
            res["Insensitivity"] = read_flags(data["Unknown6"], ["Stand", "Crouch", "Air"])
    res["Level"] = ["Low", "Middle", "High", "Crouch", "Air"][data["Unknown10"]]
    res["Group"] = data["Unknown8"] & 0xFF
    if data["HitEffect"] > 0:
        res["Type"] = "Armor"
        res["ArmorPriority"] = data["Unknown8"] >> 8
        res["ArmorEffect"] = {1: ["Grey Life", "V-Gauge Gain"], 2: ["V-Gauge Gain"], 3: ["None"]}.get(data["HitEffect"], ["Unknown!?"])
        res["ArmorCount"] = data["Unknown9"] & 0xFF
        if data["Unknown9"] >= 256:
            unk["Unknown9_HI"] = data["Unknown9"] >> 8
        res["ArmorStun"] = data["Unknown11"] & 0xFFFF
    if data["Unknown7"]:
        unk["Unknown7"] = data["Unknown7"]
    if data["Unknown11"] >> 16:
        unk["Unknown11_HI"] = data["Unknown11"] >> 16
    if data["Unknown12"] != 1:
        unk["Unknown12"] = data["Unknown12"]
    if data.get("Unknown13"):
        unk["Unknown13"] = data["Unknown13"]
    if len(unk) > 0:
        res["Unknown"] = unk
    return res
