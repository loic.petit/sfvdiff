import math

from model_transform.util import transform_time_element
from view.force_simulation import FLT


def parse_command(command, params):
    if command == (0, 1):
        return "Stance Change", {
            "Stance": ["Normal", "Alternate"][params[0]]
        }
    if command == (0, 2):
        parsed_params = {"Activate": ["Off", "On"][params[0]]}
        if params[0]:
            parsed_params["Script"] = params[1]
        if params[2]:
            parsed_params["ScriptTime"] = params[2]
        return "Opponent Transition", parsed_params
    if command == (0, 3):
        return "Spawn Projectile", {
            "Script": params[0],
            "XShift": params[1] / 100,
            "YShift": params[2] / 100
        }
    if command == (0, 4):
        return "Throw Tech", {
            "Buffer": params[0]
        }
    if command == (0, 5):
        return "Manual Control", {
            "RightDirectionAmount": params[0] / 100,
            "LeftDirectionAmount": params[1] / 100
        }
    if command == (0, 6):
        return "Opponent Homing", {
            "BaseXShift": params[0] / 100,
            "BaseYShift": params[1] / 100
        }
    if command == (0, 7):
        return "Wall Jump Transition", {
            "Script": params[0],
            "WallProximity": params[1] / 100
        }
    if command == (0, 8):
        return "Camera Wall Homing", {
            "FrameLength": params[0],
            "VerticalSpeed": params[1] / 100,
            "WallSide": [None, "Left", "Right"][params[2]]
        }
    if command == (0, 9):
        return "Position Reset", {
            "UK0": params[0],
            "UK3": params[3]
        }
    if command == (0, 10):
        parsed_params = {
            "Set": ["Stop Script", "Start Script"][params[0]],
            "Script": params[1]
        }
        if params[2]:
            parsed_params["ScriptTime"] = params[2]
        return "Nadeshiko Control", parsed_params
    if command == (0, 11):
        return "Gauge Control", {
            "Target": ["Self", "Opponent"][params[0]],
            "Gauge": ["UnknownGauge0", "EX", "V-Gauge", "UnknownGauge3", "Stun", "UnknownGauge5", "V-Trigger", "UnknownGauge7", "UnknownGauge8"][
                params[1]],
            "Increment": params[2]
        }
    if command == (0, 12):
        return "Position Change", {
            "Type": params[0],
            "XShift": params[3],
            "YShift": params[4]
        }
    if command == (1, 0):
        if not params[0]:
            return "Change Execution Speed", {"Disabled": "Yes"}
        if params[3]:
            total = FLT(params[1] * params[2]) / FLT(params[3])
        else:
            total = FLT(params[1])

        p1_speed = FLT(params[2]) / total
        p2_speed = FLT(params[3]) / total
        duration = math.ceil(total) + 2
        p1_duration = p1_speed * duration
        p2_duration = p2_speed * duration

        res = {
            "TotalFrames": duration,
            "SelfTicks": math.ceil(p1_duration),
            "SelfSpeed": "%d%%" % round(p1_speed * 100),
            "OpponentTicks": math.ceil(p2_duration),
            "OpponentSpeed": "%d%%" % round(p2_speed * 100)
        }
        if params[4] == 0:
            res["Dark"] = "Yes"
        return "Change Execution Speed", res

    if command == (1, 1):
        if not params[0]:
            return "Slowdown", {"Disable": "Yes"}
        if params[2]:
            total = FLT(params[1] * 60) / FLT(params[2])
        else:
            total = FLT(params[1])

        p1_speed = FLT(params[2]) / FLT(60)
        p2_speed = FLT(params[3]) / FLT(60)
        duration = math.ceil(total) + 2
        p1_duration = p1_speed * duration
        p2_duration = p2_speed * duration

        res = {
            "Replace": "Yes" if params[0] == 2 else "No",
            "TotalFrames": duration,
            "SelfTicks": math.ceil(p1_duration),
            "SelfSpeed": "%d%%" % round(p1_speed * 100),
            "OpponentTicks": math.ceil(p2_duration),
            "OpponentSpeed": "%d%%" % round(p2_speed * 100)
        }
        if params[4]:
            res["ShadeTransitionFrames"] = params[4] if params[4] > 0 else (256+params[4])
        return "Slowdown", res
    return "Unknown (%d, %d)" % command, {"Params": params}


def transform_other(data):
    res = transform_time_element(data)
    command = data["Unknown1"], data["Unknown2"]
    command, params = parse_command(command, data["Ints"])
    res["Command"] = command
    res["Parameters"] = params
    return res
