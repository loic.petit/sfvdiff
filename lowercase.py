import os
import pickle
import re

from deepdiff import DeepDiff
from diff_transform import transform_diff
from list_diff import clean_diff

from load import load_char

output_path = "result/"
char_path = "D:/Steam/SteamApps/common/StreetFighterV/StreetFighterV/Content/Paks/moves/StreetFighterV/Content/Chara"
chars = sorted(a for a in os.listdir(char_path) if a != "NDK")
versions = {re.sub("s([0-9\.]*)-.*","\\1", version): {line[:3]: line[4:] for line in open("../sfv-simulator/versions/"+version).read().strip().split("\n")} for version in os.listdir("../sfv-simulator/versions") if version[-3:] == "txt"}
for c in chars:
    print(c)
    previous = None
    previous_data = None
    for version, by_char in sorted(versions.items()):
        if version != '5.001':
            continue
        current = by_char.get(c)
        if current == previous:
            continue

        current_data = load_char(char_path, c, current)
        print("\t", version, current)
        for m in current_data["BCM"]["Moves"].keys():
            if re.match(".*[a-z].*", m):
                print("Found", m)
print(sorted(versions.keys()))
exit(0)
# diff = {}
